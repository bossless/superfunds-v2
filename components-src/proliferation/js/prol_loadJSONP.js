define( function ( require ) {

    'use strict';

    return function ( url, callbackName, callback, errback ) {
        var script;

        window[ callbackName ] = callback;

        script = document.createElement('script');
        script.src = url;

        script.onload = function () {
            script.parentNode.removeChild( script );
        };

        script.onerror = function ( err ) {
            if ( errback ) {
                errback( err );
            } else {
                throw err;
            }
        };

        document.getElementsByTagName( 'head' )[0].appendChild( script );
    }

});
