(function () {

    var config, localRequire, launch;

    config = {
        context: 'carbon',
        baseUrl: '<%= components %>/carbon/',

        paths: {
            d3: '<%= assets %>/lib/d3.min',
            jquery: '<%= assets %>/lib/jquery.min'
        }
    };

    launch = function ( carbon ) {
        var target = document.getElementById( 'gia-carbon' ),
            baseUrl = '<%= projectUrl %>';

        carbon.launch( target, baseUrl, '<%= assets %>' );
    };

    if ( window.GIA_LOADER_IS_REQUIRE ) {
        localRequire = require.config( config );
        localRequire([ 'carbon' ], launch );
    } else {
        require( config, [ 'carbon' ], launch );
    }

}());
