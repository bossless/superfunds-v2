define( function ( require ) {

	'use strict';

	var Vector = require( 'lib/Vector' );

	var Line = function ( a, b ) {
		var dx, dy;

		dx = b.x - a.x;
		dy = b.y - a.y;

		if ( !dx ) {
			this.x = a.x;
			this.vertical = true;
			return;
		}

		this.gradient = dy / dx;
		this.base = a.y - ( this.gradient * a.x );
	};

	Line.prototype = {
		intersection: function ( line2 ) {
			var line1 = this, relativeGradient, baseDifference, x, y;

			if ( line1.gradient === line2.gradient ) {
				// parallel lines don't intersect
				return null;
			}

			if ( line1.vertical ) {
				return new Vector( line1.x, line2.getY( line1.x ) );
			}

			if ( line2.vertical ) {
				return new Vector( line2.x, line1.getY( line2.x ) );
			}


			relativeGradient = line2.gradient - line1.gradient;
			baseDifference = line2.base - line1.base;

			x = -( baseDifference / relativeGradient );
			y = line1.getY( x );

			return new Vector( x, y );
		},

		getX: function ( y ) {
			if ( this.vertical ) {
				return this.x;
			}

			return ( y - this.base ) / this.gradient;
		},

		getY: function ( x ) {
			if ( this.vertical ) {
				return null;
			}

			return this.base + this.gradient * x;
		}
	};

	return Line;

});
