define( function ( require ) {

	'use strict';

	var throttle = require( 'utils/timing/throttle' );

	return {
		init: function ( app ) {
			if ( window.STANDALONE ) {
				app.trigger( 'start' );
				return;
			}

			var scrollHandler = throttle( function () {
				var bcr = app.el.getBoundingClientRect();

				// if the bottom of the block is visible, initialise
				if ( bcr.bottom < window.innerHeight ) {
					app.trigger( 'start' );
					window.removeEventListener( 'scroll', scrollHandler );
				}
			});

			window.addEventListener( 'scroll', scrollHandler );
		}
	};

});