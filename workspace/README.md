This folder is a place to store things while you're working on the project (e.g. scripts, artwork and so on) but which aren't part of the project itself.

The `untracked` folder is ignored by git, and is therefore a good place to put large files (e.g. photos).
