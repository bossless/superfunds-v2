/*global define */
define( function () {

	'use strict';

	return {
		boot: function ( el ) {

			var supported, loadCSS, loadJS, head, localRequire, launch, scripts, loadNextScript, environmentIsNG;

			el.id = 'gia-superfunds';

			// load CSS
			head = document.getElementsByTagName( 'head' )[0];

			loadCSS = function ( url ) {
				var link;

				// If the stylesheet already exists in the page (e.g. multiple components loading the same
				// fonts.css file), abort
				if ( document.querySelector( 'link[href="' + url + '"]' ) ) {
					return;
				}

				link = document.createElement( 'link' );
				link.setAttribute( 'rel', 'stylesheet' );
				link.setAttribute( 'href', url );

				head.appendChild( link );
			};

			//loadCSS( '<%= fonts %>' );
			loadCSS( '<%= projectUrl %>/<%= versionDir %>/styles/min.css' );
			loadCSS( '<%= projectUrl %>/<%= versionDir %>/styles/components.css' );


			loadJS = function ( url, callback ) {
				var script;

				script = document.createElement( 'script' );
				script.src = url;

				script.onload = callback;

				head.appendChild( script );
			};

			window.GIA_LOADER_IS_REQUIRE = typeof require() === 'function';

			scripts = [
				'<%= projectUrl %>/<%= versionDir %>/components/toxictrail/boot.js',
				'<%= projectUrl %>/<%= versionDir %>/components/map/boot.js',
				'<%= projectUrl %>/<%= versionDir %>/components/video/boot.js',
				'<%= projectUrl %>/<%= versionDir %>/components/carbon/boot.js',
				'<%= projectUrl %>/<%= versionDir %>/components/oversight/boot.js',
				'<%= projectUrl %>/<%= versionDir %>/components/proliferation/boot.js'
			];

			environmentIsNG = el.tagName === 'FIGURE'; // brittle as fuck... but we're out of options

			if ( environmentIsNG ) {
				scripts.unshift( '<%= projectUrl %>/<%= versionDir %>/js/app.js' );
			}

			loadNextScript = function () {
				var url;

				if ( url = scripts.shift() ) {
					setTimeout( function () {
						loadJS( url, loadNextScript );
					}, 0 );
				}
			};

			loadNextScript();
		}
	};

});
