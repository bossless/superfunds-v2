loaders
=======

This folder is for AMD loaders. These allow you to request resources asychronously during development (e.g. an HTML file containing scaffolding for your app) but bundle them up for deployment with the require.js optimiser.

Make sure your AMD paths config (found in Gruntfile.js, in the `requirejs` section) knows where to find these loaders. It's best not to put them in the root, because a) clutter and b) RequireJS and Curl have different ideas about where to find them (Curl is used on the next-gen website).
