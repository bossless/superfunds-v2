module.exports = function ( grunt ) {
	return {
		manifest: {
			files: [{
				expand: true,
				cwd: 'tmp/new',
				src: [ '<%= prod ? "**" : "nope" %>' ],
				dest: 'build/<%= target %>/'
			}]
		},
		versioned: {
			files: [{
				expand: true,
				cwd: 'src/versioned/files',
				src: [ '**', '!js/**/*' ],
				dest: 'build/<%= target %>/v/x/files/'
			}]
		},
		nonVersioned: {
			files: [{
				expand: true,
				cwd: 'src/',
				src: [ '**', '!versioned', '!versioned/**/*', '<%= prod ? "!" : "" %>*.html', 'index.html' ],
				dest: 'build/<%= target %>/'
			}]
		},
		js: {
			files: [{
				expand: true,
				cwd: 'src/versioned/js',
				src: [ '**' ],
				dest: 'build/<%= tmpTarget %>/v/x/js' // tmp, as it needs to be optimised
			}]
		},
		preview: {
			files: [{
				expand: true,
				cwd: 'preview/',
				src: [ '<%= prod ? "!" : "" %>*.html', 'index.html' ],
				dest: 'build/<%= target %>/'
			}]
		},
		codeForR2: {
			files: [{
				src: '<%= prod ? "preview/codeobject.html" : "nope" %>',
				dest: 'build/code_for_R2<%= stage ? "_stage" : "" %>.html'
			}]
		},
		options: {
			processContent: function ( content, srcpath ) {
				return grunt.template.process( content );
			},

			// We manually specify which file extensions shouldn't be processed - basically
			// any binary files such as images, audio, video etc, plus readmes
			processContentExclude: [ '**/*.{jpg,png,gif,mp3,ogg,mp4}', '**/README.md' ]
		},

		component_styles: {
			src: 'build/tmp/v/x/styles/components.css',
			dest: 'build/<%= target %>/v/x/styles/components.css'
		},


		toxictrail: {
			src: 'components-src/toxictrail/js/boot.js',
			dest: 'build/<%= target %>/v/x/components/toxictrail/boot.js'
		},

		map: {
			src: 'components-src/map/js/boot.js',
			dest: 'build/<%= target %>/v/x/components/map/boot.js'
		},

		video: {
			src: 'components-src/video/js/boot.js',
			dest: 'build/<%= target %>/v/x/components/video/boot.js'
		},

		carbon: {
			src: 'components-src/carbon/js/boot.js',
			dest: 'build/<%= target %>/v/x/components/carbon/boot.js'
		},

		oversight: {
			src: 'components-src/oversight/js/boot.js',
			dest: 'build/<%= target %>/v/x/components/oversight/boot.js'
		},

		proliferation: {
			src: 'components-src/proliferation/js/boot.js',
			dest: 'build/<%= target %>/v/x/components/proliferation/boot.js'
		},

		components_to_min: {
			files: [{
				expand: true,
				cwd: 'build/tmp/v/x/components/',
				src: '**/*.js',
				dest: 'build/min/v/x/components/'
			}]
		},



		min_to_zip: {
			files: [{
				expand: true,
				cwd: 'build/min/',
				src: [ '**', '!**/*.js', '!**/*.json', '!**/*.csv', '!**/*.css', 'manifest.json' ],
				dest: 'build/zip/'
			}]
		}
	};
};
