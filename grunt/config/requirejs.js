module.exports = function ( grunt ) {

	return {
		options: {
			baseUrl: 'build/tmp/v/x/js/',
			out: 'build/min/v/x/js/app.js',
			name: 'app',
			optimize: 'none', // js gets uglified separately, no need to waste time here

			// If you change the paths config in boot.js, ensure the changes
			// are reflected here
			// paths: {
			// 	text: 'loaders/text',
			// 	rvc: 'loaders/rvc',
			// 	'amd-loader': 'loaders/amd-loader',
			// 	Ractive: 'lib/Ractive'
			// },

			// // We don't need to include loader plugins in the build
			// stubModules: [ 'text' ]//,

			// onBuildWrite: function ( name, path, contents ) {
			// 	var amdclean = require( 'amdclean' );

			// 	// exclude Ractive from build
			// 	if ( /Ractive$/.test( name ) ) {
			// 		return 'console.log("window.Ractive",window.Ractive);var Ractive = window.Ractive;';
			// 	}

			// 	return require( 'amdclean' ).clean({
			// 		code: contents
			// 	});
			// },

			// wrap: true
		},

		toxictrail: {
			options: {
				baseUrl: 'components-src/toxictrail/js',
				out: 'build/<%= tmpTarget %>/v/x/components/toxictrail/toxictrail.js',
				name: 'toxictrail',

				paths: {
					text: 'loaders/text',
					rvc: 'loaders/rvc',
					'amd-loader': 'loaders/amd-loader',
					Ractive: 'lib/Ractive'
				},

				stubModules: [ 'text', 'amd-loader', 'rvc', 'Ractive' ],

				onBuildWrite: rewriteModule
			}
		},

		map: {
			options: {
				baseUrl: 'components-src/map/js',
				out: 'build/<%= tmpTarget %>/v/x/components/map/map.js',
				name: 'map',

				paths: {
					text: 'loaders/text',
					d3: 'lib/d3',
					jquery: 'lib/jquery'
				},

				stubModules: [ 'text', 'd3', 'jquery' ],

				onBuildWrite: rewriteModule
			}
		},

		video: {
			options: {
				baseUrl: 'components-src/video/js',
				out: 'build/<%= tmpTarget %>/v/x/components/video/video.js',
				name: 'video',

				paths: {
					rvc: 'loaders/rvc',
					'amd-loader': 'loaders/amd-loader',
					Ractive: 'lib/Ractive'
				},

				stubModules: [ 'amd-loader', 'rvc', 'Ractive' ],

				onBuildWrite: rewriteModule
			}
		},

		carbon: {
			options: {
				baseUrl: 'components-src/carbon/js',
				out: 'build/<%= tmpTarget %>/v/x/components/carbon/carbon.js',
				name: 'carbon',

				paths: {
					text: 'loaders/text',
					d3: 'lib/d3',
					jquery: 'lib/jquery'
				},

				stubModules: [ 'text', 'jquery', 'd3' ],

				onBuildWrite: rewriteModule
			}
		},

		oversight: {
			options: {
				baseUrl: 'components-src/oversight/js',
				out: 'build/<%= tmpTarget %>/v/x/components/oversight/oversight.js',
				name: 'oversight',

				paths: {
					text: 'loaders/text',
					d3: 'lib/d3',
					jquery: 'lib/jquery'
				},

				stubModules: [ 'text', 'jquery', 'd3' ],

				onBuildWrite: rewriteModule
			}
		},

		proliferation: {
			options: {
				baseUrl: 'components-src/proliferation/js',
				out: 'build/<%= tmpTarget %>/v/x/components/proliferation/proliferation.js',
				name: 'proliferation',

				paths: {
					text: 'loaders/text',
					d3: 'lib/d3',
					jquery: 'lib/jquery'
				},

				stubModules: [ 'text', 'jquery', 'd3' ],

				onBuildWrite: rewriteModule
			}
		}
	};

	function rewriteModule ( name, path, contents ) {
		// don't include stub modules
		if ( /define\('[^']+',\{\}\);/.test( contents ) ) {
			return '';
		}

		if ( /"Dynamic load not allowed: "/.test( contents ) ) {
			return '';
		}

		var rewritten = contents.replace( /["']([a-z]+)\!([^"']+)["']/g, function ( match, loader, name ) {
			var pathParts, resolvedPath, result;

			path = path.replace( /\/.+?\/components-src\/[a-z]+\/js\//, '' ).replace( loader + '!', '' );

			if ( /\.\//.test( name ) ) {
				pathParts = path.split( '/' );
				pathParts.pop();

				if ( pathParts.length ) {
					resolvedPath = pathParts.join( '/' ) + '/' + name.substring( 2 );
				} else {
					resolvedPath = name.substring( 2 );
				}
			}

			result = '"' + loader + '_' + (resolvedPath || name) + '"';
			return result;
		});

		return grunt.template.process( rewritten );
	}

};
