module.exports = {
	options: {
		debugInfo: '<%= prod ? false : true %>',
		style: ( '<%= min ? "compressed" : "expanded" %>' )
	},

	main: {
		src: 'src/versioned/styles/main.scss',
		dest: 'build/<%= target %>/v/x/styles/min.css'
	},

	components: {
		src: 'components-src/components.scss',
		dest: 'build/tmp/v/x/styles/components.css'
	}
};
