module.exports = function ( grunt ) {

	'use strict';

	grunt.registerTask( 'buildComponents', [
		'requirejs:toxictrail',
		'copy:toxictrail',

		'requirejs:map',
		'copy:map',

		'requirejs:video',
		'copy:video',

		'requirejs:carbon',
		'copy:carbon',

		'requirejs:oversight',
		'copy:oversight',

		'requirejs:proliferation',
		'copy:proliferation',

		'sass:components'
	]);

};
