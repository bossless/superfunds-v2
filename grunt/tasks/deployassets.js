module.exports = function ( grunt ) {

	'use strict';

	// Set flags (so template tags evaluate correctly)
	grunt.registerTask( 'deployassets', [
		'aws_s3:uploadAssets'
	]);

};
