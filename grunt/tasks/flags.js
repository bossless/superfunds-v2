module.exports = function ( grunt ) {

	'use strict';

	// Set flags (so template tags evaluate correctly)
	grunt.registerTask( 'setDryrunFlag', function () {
		grunt.config( 'dryrun', true );
	});

	grunt.registerTask( 'setStageFlag', function () {
		grunt.config( 'stage', true );
	});

	grunt.registerTask( 'setProdFlag', function () {
		grunt.config( 'prod', true );
		grunt.config( 'min', true );
	});

	grunt.registerTask( 'setMinFlag', function () {
		grunt.config( 'min', true );
	});

};
