module.exports = function ( grunt ) {

	'use strict';

	grunt.registerTask( 'buildToxictrail', function () {
		var rjs = require( 'requirejs' );

		rjs.optimize({
			baseUrl: 'components-src/toxictrail/',
			out: 'components/toxictrail/app.js',
			name: 'app',
			optimize: 'none', // js gets uglified separately, no need to waste time here

			// If you change the paths config in boot.js, ensure the changes
			// are reflected here
			paths: {
				text: 'loaders/text' // necessary for curl, which expects plugins in a specific folder
			},

			// We don't need to include loader plugins in the build
			stubModules: [ 'text' ]
		}, function ( result ) {

		}, function ( err ) {

		});

	});

};
