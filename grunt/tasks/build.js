module.exports = function ( grunt ) {

	'use strict';

	var buildSequence = [
		'clean:build',

		'jshint',
		'copy',
		'sass',
		'spelunk',

		'buildArticle',
		'buildComponents',

		'copy:components_to_min',
		'copy:component_styles'
	];

	grunt.registerTask( 'build', buildSequence );
	grunt.registerTask( 'min', [ 'setMinFlag' ].concat( buildSequence ).concat([ 'requirejs', 'cssmin', 'uglify' ]) );

};
